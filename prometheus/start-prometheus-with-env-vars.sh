#! /bin/sh

env | while IFS='=' read -r n v
do
    if [ ! "${n%%PROMETHEUS*}" ]
    then
        sed -i -e "s#\${${n#PROMETHEUS_}}#${v}#g" /etc/prometheus/prometheus.yml
    fi
done

/bin/prometheus "$@"
